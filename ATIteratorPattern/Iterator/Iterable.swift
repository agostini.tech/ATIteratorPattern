//
//  Iterable.swift
//  ATIteratorPattern
//
//  Created by Dejan on 09/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Iterable {
    func makeIterator() -> VehicleIterator
}

class VehicleQueue: Iterable
{
    private let queue: LinkedList<Vehicle>
    init(_ queue: LinkedList<Vehicle>) {
        self.queue = queue
    }
    
    // Your custom business logic goes here...
    
    func makeIterator() -> VehicleIterator {
        return LinkedListVehicleIterator(queue)
    }
}

class Factory: Iterable
{
    private let items: [ManufacturedVehicle]
    init(_ items: [ManufacturedVehicle]) {
        self.items = items
    }
    
    // This class could be responsible for producing vehicles, for example...
    
    func makeIterator() -> VehicleIterator {
        return ManufacturedVehicleIterator(items)
    }
}

class ParkingLot: Iterable
{
    private let vehicles: [Vehicle]
    init(_ vehicles: [Vehicle]) {
        self.vehicles = vehicles
    }
    
    // Your parking lot, vehicles come and go as they please...
    
    func makeIterator() -> VehicleIterator {
        return ArrayVehicleIterator(vehicles)
    }
}

class Garage: Iterable
{
    private let cars: [String : Vehicle]
    init(_ cars: [String : Vehicle]) {
        self.cars = cars
    }
    
    // Might be your personal garage, might be your neighbours'... This is another class that will contain your business logic...
    
    func makeIterator() -> VehicleIterator {
        return DictionaryVehicleIterator(cars)
    }
}
