//
//  Vehicle.swift
//  ATIteratorPattern
//
//  Created by Dejan on 09/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Vehicle
{
    var id: Int { get }
    var name: String { get }
    var make: String { get }
    var model: String { get }
    var registration: String { get }
}

protocol ManufacturedVehicle
{
    var date: Date { get }
    var assemblyLine: String { get }
    var passedQA: Bool { get }
    var qaPerformedBy: String { get }
    var vehicle: Vehicle { get }
}

class VehiclesFactory
{
    static func getVehiclesArray() -> [Vehicle] {
        return [
            VehicleItem(id: 0, name: "Red Car", make: "Ford", model: "Mondeo", registration: "123aa4"),
            VehicleItem(id: 1, name: "Blue Car", make: "Ford", model: "Fiesta", registration: "432dd"),
            VehicleItem(id: 2, name: "Green Car", make: "Nissan", model: "Leaf", registration: "hhg654"),
            VehicleItem(id: 3, name: "Rusty Car", make: "Ford", model: "T", registration: "855tes"),
            VehicleItem(id: 4, name: "Red Pickup", make: "Toyota", model: "Hilux", registration: "kjjg665"),
            VehicleItem(id: 5, name: "Jude's Candy Van", make: "Toyota", model: "Hiace", registration: "3321jjg"),
        ]
    }
    
    static func getFactoryVehicles() -> [ManufacturedVehicle] {
        return [
            FactoryVehicle(date: Date(), assemblyLine: "Detroid", passedQA: true, qaPerformedBy: "Jude", vehicle: VehicleItem(id: 9, name: "New Shiny Car", make: "GM", model: "Top Secret", registration: "xx4433")),
            FactoryVehicle(date: Date(timeIntervalSinceNow: -1000), assemblyLine: "Mexico", passedQA: true, qaPerformedBy: "Nenad", vehicle: VehicleItem(id: 88, name: "Fiery Red", make: "Opel", model: "Astra", registration: "55422")),
            FactoryVehicle(date: Date(timeIntervalSinceNow: -4444), assemblyLine: "Germany", passedQA: false, qaPerformedBy: "Hans", vehicle: VehicleItem(id: 66, name: "Space Car", make: "BMW", model: "X0", registration: "Mars-1"))
        ]
    }
    
    static func getVehiclesList() -> LinkedList<Vehicle> {
        let head = LinkedList<Vehicle>(VehicleItem(id: 44, name: "Pac Man", make: "Mini", model: "Cooper", registration: "887744da"))
        head.nextItem = LinkedList<Vehicle>(VehicleItem(id: 33, name: "Little Thumper", make: "Rolls Royce", model: "Royal", registration: "blue1"))
        head.nextItem?.nextItem = LinkedList<Vehicle>(VehicleItem(id: 90, name: "Green Beast", make: "Ford", model: "Mustang", registration: "4213245"))
        head.nextItem?.nextItem?.nextItem = LinkedList<Vehicle>(VehicleItem(id: 77, name: "Big Blue", make: "Smart", model: "forTwo", registration: "09876"))
        return head
    }
    
    static func getVehiclesDictionary() -> [String : Vehicle] {
        return [
            "slot 1" : VehicleItem(id: 55, name: "My Car", make: "Lada", model: "Niva", registration: "554433"),
            "slot 2" : VehicleItem(id: 66, name: "Not My Car", make: "Lamborghini", model: "Diablo", registration: "RichInSpirit")
        ]
    }
}

fileprivate struct VehicleItem: Vehicle
{
    var id: Int
    var name: String
    var make: String
    var model: String
    var registration: String
}

fileprivate struct FactoryVehicle: ManufacturedVehicle
{
    var date: Date
    var assemblyLine: String
    var passedQA: Bool
    var qaPerformedBy: String
    var vehicle: Vehicle
}
