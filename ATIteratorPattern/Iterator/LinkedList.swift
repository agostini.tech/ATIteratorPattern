//
//  LinkedList.swift
//  ATIteratorPattern
//
//  Created by Dejan on 09/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

class LinkedList<T>
{
    let item: T
    var nextItem: LinkedList<T>?
    
    init(_ item: T) {
        self.item = item
    }
}
