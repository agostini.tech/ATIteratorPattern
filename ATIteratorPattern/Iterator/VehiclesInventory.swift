//
//  VehiclesInventory.swift
//  ATIteratorPattern
//
//  Created by Dejan on 09/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

class VehiclesInventory
{
    // Create the test objects... Usually you would inject these in the constructor.
    private let queue = VehicleQueue(VehiclesFactory.getVehiclesList())
    private let factory = Factory(VehiclesFactory.getFactoryVehicles())
    private let parking = ParkingLot(VehiclesFactory.getVehiclesArray())
    private let garage = Garage(VehiclesFactory.getVehiclesDictionary())
    
    func printVehiclesInventory() {
        print("==========QUEUE============")
        printVehicles(iterator: queue.makeIterator())
        print("==========FACTORY==========")
        printVehicles(iterator: factory.makeIterator())
        print("==========PARKING==========")
        printVehicles(iterator: parking.makeIterator())
        print("==========GARAGE===========")
        printVehicles(iterator: garage.makeIterator())
    }
    
    private func printVehicles(iterator: VehicleIterator) {
        while let item = iterator.next() {
            print(item.name)
        }
    }
}
