//
//  Iterator.swift
//  ATIteratorPattern
//
//  Created by Dejan on 09/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol VehicleIterator
{
    func next() -> Vehicle?
}

class LinkedListVehicleIterator: VehicleIterator
{
    private var cursor: LinkedList<Vehicle>?
    private let items: LinkedList<Vehicle>
    init(_ items: LinkedList<Vehicle>) {
        self.items = items
        self.cursor = self.items
    }
    
    func next() -> Vehicle? {
        let vehicle = self.cursor?.item
        self.cursor = self.cursor?.nextItem
        return vehicle
    }
}

class ArrayVehicleIterator: VehicleIterator
{
    private var cursor: Int?
    private let items: [Vehicle]
    init(_ items: [Vehicle]) {
        self.items = items
    }
    
    func next() -> Vehicle? {
        if let idx = getNextCursor(cursor) {
            self.cursor = idx
            return self.items[idx]
        }
        return nil
    }
    
    private func getNextCursor(_ cursor: Int?) -> Int? {
        if var idx = cursor, idx < items.count - 1 {
            idx += 1
            return idx
        } else if cursor == nil, items.count > 0 {
            return 0
        } else {
            return nil
        }
    }
}

class DictionaryVehicleIterator: ArrayVehicleIterator
{
    init(_ items: [String: Vehicle]) {
        let items: [Vehicle] = Array(items.values)
        super.init(items)
    }
}

class ManufacturedVehicleIterator: ArrayVehicleIterator
{
    init(_ items: [ManufacturedVehicle]) {
        let items: [Vehicle] = items.map { $0.vehicle }
        super.init(items)
    }
}
